import { useEffect, useState } from 'react';
import './Register.css';

function Register(props) {
  
  const [ideoma, setIdeoma] = useState("español")
  const [estaEnEspañol, setEstaEnEspañol] = useState(false)
  const [apellido, setApellido] = useState()
  const [nombre, setNombre] = useState()

  const handleApellido = (e) => {
    setApellido(e.target.value)
  }
  const handleNombre = (e) => {
    setNombre(e.target.value)
  }
  const handleAceptar = () => {
    console.log(apellido)
    console.log(nombre)
  }

  useEffect(() => {

    if (ideoma === "español"){
      setEstaEnEspañol(true)
    }
  
  })

  
return (
      <div className="Fondo">
        {props.nombreUsuario}
        {estaEnEspañol ? (<h1>la app esta en español</h1>) : (<h1>this is app in english</h1>)}
       <h2>Ingresa tus datos</h2>
       <p>Ingresa tu nombre</p>
       <input onChange={(e) => handleNombre(e)}></input><br/>
       <p>Ingresa tu apellido</p>
       <input onChange={(e) => handleApellido(e)}></input><br/>
       <p>Ingresa tu eddad</p>
       <input></input><br/>
       <button onClick={() => handleAceptar()}>aceptar </button>
      </div>
    );
  }

  
  export default Register;

  
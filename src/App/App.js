import {useState, useEffect} from "react"
import './App.css';
import Register from "../Register/Register"
import Texts from "../Texts"
import PieDePagina from "../PieDePagina"


function App() {
  //aca se ejecuta javaScript, se declaran variables y funciones
  const [nombreUsuario, setNombreUsuario] = useState("Gerardo")
  const [estaLoguado, setEstaLogueado] = useState(false)
  const [apellidoUsuario, setApellidoUsuario] = useState("enriquez")
  const [edadDeUsuario, setEdadDeUsuario] = useState(17)
  const [esMayorDeEdad, setEsMayorDeEdad] = useState(false)

  const calcularSiEsMayor = () => {
    if (edadDeUsuario >= 18){
      setEsMayorDeEdad(true)
    }else{
      setEsMayorDeEdad(false)
    }
  }
  
  useEffect(() => {
    //se ejecutan las funciones y los if
    calcularSiEsMayor()
  })
  
  
  return (
    <div className="App">
        <Register nombreUsuario={nombreUsuario} apellidoUsuario={apellidoUsuario} edadDeUsuario={edadDeUsuario} esMayorDeEdad={esMayorDeEdad}/>
        <Texts nombreUsuario={nombreUsuario} apellidoUsuario={apellidoUsuario} estaLoguado={estaLoguado}/>
      <h1>Su edad es:{edadDeUsuario}</h1>
      { esMayorDeEdad ? <h1>es mayor de edad </h1> : <h1>!No es mayor de edad</h1>}
     
       <PieDePagina />
     
    </div>
  );
}

export default App;
